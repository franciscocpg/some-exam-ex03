package com.francisco.guimaraes.some.exam.ex03;

import static org.junit.Assert.*;
import org.junit.Test;

/*
 * @author Francisco Guimarães
 * @since 26/06/2016
 */
public class Ex03Test {

    @Test
    public void testFirstNonRepeatingChar() throws Exception {
        /*
         * Exemplo do exercício
         */
        assertCharEquals('b', Ex03.firstNonRepeatingChar(StringStream.create("aAbBABac")));
        /*
         * Outros exemplos
         */
        assertCharEquals('b', Ex03.firstNonRepeatingChar(StringStream.create("abcdefghija")));
        assertCharEquals('h', Ex03.firstNonRepeatingChar(StringStream.create("hello")));
        assertCharEquals('J', Ex03.firstNonRepeatingChar(StringStream.create("Java")));
        assertCharEquals('i', Ex03.firstNonRepeatingChar(StringStream.create("simplest")));

        /**
         * Em caso de não localização do caracter, o sistema deve informar uma
         * mensagem amigável.
         */
        try {
            assertCharEquals('J', Ex03.firstNonRepeatingChar(StringStream.create("JaaJ")));
            throw new Exception("Deveria ser lançado uma exceção");
        } catch (Exception ex) {
            assertEquals("Não existem caracteres não repetidos na stream", ex.getMessage());
        }

        /**
         * Stream não aceita nulo.
         */
        try {
            assertCharEquals('b', Ex03.firstNonRepeatingChar(StringStream.create(null)));
            throw new Exception("Deveria ser lançado uma exceção");
        } catch (IllegalArgumentException ex) {
            assertEquals("str não pode ser nulo", ex.getMessage());
        }
    }

    /**
     * Método criado para facilitar a visualização de qual caracter era esperado
     * e qual foi recebido.
     *
     * Se chamarmos
     * {@link org.junit.Assert#assertEquals(java.lang.Object, java.lang.Object)}
     * sem a conversão para string é mostrado na saida o código ASCII do char.
     *
     * @param expected
     * @param actual
     */
    private static void assertCharEquals(char expected, char actual) {
        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }

}
