package com.francisco.guimaraes.some.exam.ex03;

/**
 *
 * @author Francisco Guimarães
 * @since 27/06/2016
 */
public interface Stream {

    public char getNext();

    public boolean hasNext();
}
