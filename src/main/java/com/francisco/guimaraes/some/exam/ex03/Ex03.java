/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.francisco.guimaraes.some.exam.ex03;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Francisco Guimarães
 * @since 26/06/2016
 */
public class Ex03 {

    private Ex03() {
    }

    /**
     * Recupera o primeiro caractere não repitido na stream.
     *
     * @param stream Stream.
     * @return O primeiro caractere não repitido na stream.
     */
    public static char firstNonRepeatingChar(Stream stream) throws Exception {
        Set<Character> repeating = new HashSet<>();
        List<Character> nonRepeating = new ArrayList<>();
        while (stream.hasNext()) {
            char letter = stream.getNext();
            if (repeating.contains(letter)) {
                continue;
            }
            if (nonRepeating.contains(letter)) {
                nonRepeating.remove((Character) letter);
                repeating.add(letter);
            } else {
                nonRepeating.add(letter);
            }
        }
        if (nonRepeating.isEmpty()) {
            throw new Exception("Não existem caracteres não repetidos na stream");
        }
        return nonRepeating.get(0);
    }

}
