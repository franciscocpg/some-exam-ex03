package com.francisco.guimaraes.some.exam.ex03;

/**
 *
 * @author Francisco Guimarães
 * @since 27/06/2016
 */
public class StringStream implements Stream {

    private final String str;
    private int pos;
    private final int len;

    private StringStream(String str) {
        this.str = str;
        this.pos = 0;
        this.len = str.length();
    }

    /**
     * Cria uma nova stream a partir da string.
     *
     * @param str string
     * @return A instância da stream.
     * @throws IllegalArgumentException Caso a str seja nula.
     */
    public static StringStream create(String str) throws IllegalArgumentException {
        if (str == null) {
            throw new IllegalArgumentException("str não pode ser nulo");
        }
        return new StringStream(str);
    }

    @Override
    public char getNext() {
        return str.charAt(pos++);
    }

    @Override
    public boolean hasNext() {
        return pos < len;
    }

}
